use quote::quote;
use syn::{
    parse_macro_input, parse_quote, Data, DeriveInput, Field, Fields, GenericParam, Generics, Meta,
    NestedMeta,
};

///
/// ```
/// use typesafe_repository_macro::Id;
/// use typesafe_repository::{GetIdentity, RefIdentity, Identity};
///
/// #[derive(Id)]
/// #[Id(ref_id, get_id)]
/// struct Foo {
///     id: u64,
/// }
///
/// #[derive(Id)]
/// struct Bar {
///     #[id]
///     test: u64,
/// }
///
/// let _a = Foo { id: 0 }.id();
/// ```
#[proc_macro_derive(Id, attributes(id, id_by, Id))]
pub fn identity_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    let name = input.ident;
    let id = parse_id(&input.data);
    let ids_by = parse_ids_by(&input.data);
    let ids_by_names: Vec<&syn::Ident> = ids_by.iter().filter_map(|id| id.ident.as_ref()).collect();
    let ids_by_types: Vec<&syn::Type> = ids_by.iter().map(|id| &id.ty).collect();
    let id_name = id.ident.as_ref().unwrap();
    let id_type = &id.ty;

    let generics = add_trait_bounds(input.generics);
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();

    let id_attr = input
        .attrs
        .iter()
        .find(|a| a.path.get_ident().map(|i| i == "Id").unwrap_or(false));

    let meta = id_attr.and_then(|a| a.parse_meta().ok());
    let get_id = match &meta {
        Some(Meta::List(l)) => l.nested.iter().cloned().find(|n| match n {
            NestedMeta::Meta(m) => m.path().get_ident().map(|x| x == "get_id").unwrap_or(false),
            _ => false,
        }),
        _ => None,
    };
    let ref_id = match &meta {
        Some(Meta::List(l)) => l.nested.iter().cloned().find(|n| match n {
            NestedMeta::Meta(m) => m.path().get_ident().map(|x| x == "ref_id").unwrap_or(false),
            _ => false,
        }),
        _ => None,
    };

    let get_id = get_id.map(|_| quote! {
        impl #impl_generics GetIdentity for #name #ty_generics #where_clause {
            fn id(&self) -> Self::Id {
                self.#id_name.clone()
            }
        }
    });

    let ref_id = ref_id.map(|_| quote! {
        impl #impl_generics RefIdentity for #name #ty_generics #where_clause {
            fn id_ref(&self) -> &Self::Id {
                &self.#id_name
            }
        }
    });

    let expanded = quote! {
        impl #impl_generics Identity for #name #ty_generics #where_clause {
            type Id = #id_type;
        }

        #get_id

        #ref_id

        #(
            impl IdentityBy<#ids_by_types> for #name {
                fn id_by(&self) -> #ids_by_types {
                    self.#ids_by_names.clone()
                }
            }
        )*
    };

    proc_macro::TokenStream::from(expanded)
}

fn add_trait_bounds(mut generics: Generics) -> Generics {
    for param in &mut generics.params {
        if let GenericParam::Type(ref mut type_param) = *param {
            type_param.bounds.push(parse_quote!(heapsize::HeapSize));
        }
    }
    generics
}

fn parse_id(data: &Data) -> &Field {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => fields
                .named
                .iter()
                .find(|f| match f.ident.as_ref().map(|i| i == "id") {
                    Some(true) => true,
                    _ => f
                        .attrs
                        .iter()
                        .find(|a| a.path.is_ident("id"))
                        .map(|_| true)
                        .unwrap_or(false),
                })
                .expect("No id field found"),
            _ => unimplemented!(),
        },
        _ => panic!(),
    }
}

fn parse_ids_by(data: &Data) -> Vec<&Field> {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => fields
                .named
                .iter()
                .filter(|f| f.attrs.iter().any(|a| a.path.is_ident("id_by")))
                .collect(),
            _ => panic!(),
        },
        _ => panic!(),
    }
}
